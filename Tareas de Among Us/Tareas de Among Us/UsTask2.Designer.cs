﻿
namespace Tareas_de_Among_Us
{
    partial class UsTask2
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsTask2));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Pnlbutton = new System.Windows.Forms.PictureBox();
            this.btndownload = new System.Windows.Forms.Button();
            this.pnlgif = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Pnlbutton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlgif)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.625F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.375F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(857, 640);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Lucida Fax", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(851, 132);
            this.label1.TabIndex = 0;
            this.label1.Text = "TAREA NUMERO 2";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btndownload);
            this.panel1.Controls.Add(this.Pnlbutton);
            this.panel1.Controls.Add(this.pnlgif);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 135);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(851, 502);
            this.panel1.TabIndex = 1;
            // 
            // Pnlbutton
            // 
            this.Pnlbutton.Image = ((System.Drawing.Image)(resources.GetObject("Pnlbutton.Image")));
            this.Pnlbutton.Location = new System.Drawing.Point(121, 71);
            this.Pnlbutton.Name = "Pnlbutton";
            this.Pnlbutton.Size = new System.Drawing.Size(643, 345);
            this.Pnlbutton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Pnlbutton.TabIndex = 0;
            this.Pnlbutton.TabStop = false;
            // 
            // btndownload
            // 
            this.btndownload.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btndownload.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btndownload.FlatAppearance.BorderSize = 2;
            this.btndownload.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndownload.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndownload.Location = new System.Drawing.Point(387, 311);
            this.btndownload.Name = "btndownload";
            this.btndownload.Size = new System.Drawing.Size(113, 27);
            this.btndownload.TabIndex = 1;
            this.btndownload.Text = "Download";
            this.btndownload.UseVisualStyleBackColor = false;
            this.btndownload.Click += new System.EventHandler(this.btndownload_Click);
            // 
            // pnlgif
            // 
            this.pnlgif.Image = ((System.Drawing.Image)(resources.GetObject("pnlgif.Image")));
            this.pnlgif.Location = new System.Drawing.Point(121, 71);
            this.pnlgif.Name = "pnlgif";
            this.pnlgif.Size = new System.Drawing.Size(643, 345);
            this.pnlgif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pnlgif.TabIndex = 2;
            this.pnlgif.TabStop = false;
            this.pnlgif.Visible = false;
            // 
            // UsTask2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UsTask2";
            this.Size = new System.Drawing.Size(857, 640);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Pnlbutton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlgif)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btndownload;
        private System.Windows.Forms.PictureBox Pnlbutton;
        private System.Windows.Forms.PictureBox pnlgif;
    }
}
