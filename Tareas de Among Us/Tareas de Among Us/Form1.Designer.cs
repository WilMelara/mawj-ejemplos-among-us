﻿
namespace Tareas_de_Among_Us
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbTask6 = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rbTask5 = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.rbTask4 = new System.Windows.Forms.RadioButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rbTask3 = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.rbTask2 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rbTask1 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbHome1 = new System.Windows.Forms.RadioButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbHome = new System.Windows.Forms.RadioButton();
            this.usTask61 = new Tareas_de_Among_Us.UsTask6();
            this.usTask51 = new Tareas_de_Among_Us.UsTask5();
            this.usTask41 = new Tareas_de_Among_Us.UsTask4();
            this.usTask31 = new Tareas_de_Among_Us.UsTask3();
            this.usTask21 = new Tareas_de_Among_Us.UsTask2();
            this.usHome1 = new Tareas_de_Among_Us.UsHome();
            this.usTask11 = new Tareas_de_Among_Us.UsTask1();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(263, 820);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbTask6);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.rbTask5);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.rbTask4);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Controls.Add(this.rbTask3);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.rbTask2);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.rbTask1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.rbHome1);
            this.panel2.Location = new System.Drawing.Point(4, 200);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(258, 608);
            this.panel2.TabIndex = 1;
            // 
            // rbTask6
            // 
            this.rbTask6.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask6.BackColor = System.Drawing.Color.DimGray;
            this.rbTask6.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask6.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask6.Image = ((System.Drawing.Image)(resources.GetObject("rbTask6.Image")));
            this.rbTask6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask6.Location = new System.Drawing.Point(0, 510);
            this.rbTask6.Name = "rbTask6";
            this.rbTask6.Size = new System.Drawing.Size(258, 67);
            this.rbTask6.TabIndex = 12;
            this.rbTask6.TabStop = true;
            this.rbTask6.Text = "TAREA N°6";
            this.rbTask6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask6.UseVisualStyleBackColor = false;
            this.rbTask6.CheckedChanged += new System.EventHandler(this.rbTask6_CheckedChanged);
            // 
            // panel8
            // 
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 498);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(258, 12);
            this.panel8.TabIndex = 11;
            // 
            // rbTask5
            // 
            this.rbTask5.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask5.BackColor = System.Drawing.Color.DimGray;
            this.rbTask5.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask5.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask5.Image = ((System.Drawing.Image)(resources.GetObject("rbTask5.Image")));
            this.rbTask5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask5.Location = new System.Drawing.Point(0, 436);
            this.rbTask5.Name = "rbTask5";
            this.rbTask5.Size = new System.Drawing.Size(258, 62);
            this.rbTask5.TabIndex = 10;
            this.rbTask5.TabStop = true;
            this.rbTask5.Text = "TAREA N°5";
            this.rbTask5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask5.UseVisualStyleBackColor = false;
            this.rbTask5.CheckedChanged += new System.EventHandler(this.rbTask5_CheckedChanged);
            // 
            // panel7
            // 
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 424);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(258, 12);
            this.panel7.TabIndex = 9;
            // 
            // rbTask4
            // 
            this.rbTask4.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask4.BackColor = System.Drawing.Color.DimGray;
            this.rbTask4.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask4.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask4.Image = ((System.Drawing.Image)(resources.GetObject("rbTask4.Image")));
            this.rbTask4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask4.Location = new System.Drawing.Point(0, 362);
            this.rbTask4.Name = "rbTask4";
            this.rbTask4.Size = new System.Drawing.Size(258, 62);
            this.rbTask4.TabIndex = 8;
            this.rbTask4.TabStop = true;
            this.rbTask4.Text = "TAREA N°4";
            this.rbTask4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask4.UseVisualStyleBackColor = false;
            this.rbTask4.CheckedChanged += new System.EventHandler(this.rbTask4_CheckedChanged);
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 350);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(258, 12);
            this.panel6.TabIndex = 7;
            // 
            // rbTask3
            // 
            this.rbTask3.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask3.BackColor = System.Drawing.Color.DimGray;
            this.rbTask3.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask3.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask3.Image = ((System.Drawing.Image)(resources.GetObject("rbTask3.Image")));
            this.rbTask3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask3.Location = new System.Drawing.Point(0, 288);
            this.rbTask3.Name = "rbTask3";
            this.rbTask3.Size = new System.Drawing.Size(258, 62);
            this.rbTask3.TabIndex = 6;
            this.rbTask3.TabStop = true;
            this.rbTask3.Text = "TAREA N°3";
            this.rbTask3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask3.UseVisualStyleBackColor = false;
            this.rbTask3.CheckedChanged += new System.EventHandler(this.rbTask3_CheckedChanged);
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 276);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(258, 12);
            this.panel5.TabIndex = 5;
            // 
            // rbTask2
            // 
            this.rbTask2.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask2.BackColor = System.Drawing.Color.DimGray;
            this.rbTask2.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask2.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask2.Image = ((System.Drawing.Image)(resources.GetObject("rbTask2.Image")));
            this.rbTask2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask2.Location = new System.Drawing.Point(0, 214);
            this.rbTask2.Name = "rbTask2";
            this.rbTask2.Size = new System.Drawing.Size(258, 62);
            this.rbTask2.TabIndex = 4;
            this.rbTask2.TabStop = true;
            this.rbTask2.Text = "TAREA N°2";
            this.rbTask2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask2.UseVisualStyleBackColor = false;
            this.rbTask2.CheckedChanged += new System.EventHandler(this.rbTask2_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 202);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(258, 12);
            this.panel4.TabIndex = 3;
            // 
            // rbTask1
            // 
            this.rbTask1.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbTask1.BackColor = System.Drawing.Color.DimGray;
            this.rbTask1.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbTask1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbTask1.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTask1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbTask1.Image = ((System.Drawing.Image)(resources.GetObject("rbTask1.Image")));
            this.rbTask1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbTask1.Location = new System.Drawing.Point(0, 140);
            this.rbTask1.Name = "rbTask1";
            this.rbTask1.Size = new System.Drawing.Size(258, 62);
            this.rbTask1.TabIndex = 2;
            this.rbTask1.TabStop = true;
            this.rbTask1.Text = "TAREA N°1";
            this.rbTask1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTask1.UseVisualStyleBackColor = false;
            this.rbTask1.CheckedChanged += new System.EventHandler(this.rbTask1_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 128);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(258, 12);
            this.panel3.TabIndex = 1;
            // 
            // rbHome1
            // 
            this.rbHome1.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbHome1.BackColor = System.Drawing.Color.DimGray;
            this.rbHome1.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbHome1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbHome1.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHome1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbHome1.Image = ((System.Drawing.Image)(resources.GetObject("rbHome1.Image")));
            this.rbHome1.Location = new System.Drawing.Point(0, 0);
            this.rbHome1.Name = "rbHome1";
            this.rbHome1.Size = new System.Drawing.Size(258, 128);
            this.rbHome1.TabIndex = 0;
            this.rbHome1.TabStop = true;
            this.rbHome1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbHome1.UseVisualStyleBackColor = false;
            this.rbHome1.CheckedChanged += new System.EventHandler(this.rbHome1_CheckedChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(22, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(224, 185);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(280, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(857, 820);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // rbHome
            // 
            this.rbHome.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbHome.BackColor = System.Drawing.Color.DimGray;
            this.rbHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.rbHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbHome.Font = new System.Drawing.Font("Lucida Fax", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbHome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rbHome.Image = ((System.Drawing.Image)(resources.GetObject("rbHome.Image")));
            this.rbHome.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rbHome.Location = new System.Drawing.Point(0, 12);
            this.rbHome.Name = "rbHome";
            this.rbHome.Size = new System.Drawing.Size(258, 62);
            this.rbHome.TabIndex = 2;
            this.rbHome.TabStop = true;
            this.rbHome.Text = "TAREA N°1";
            this.rbHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbHome.UseVisualStyleBackColor = false;
            // 
            // usTask61
            // 
            this.usTask61.Location = new System.Drawing.Point(288, 138);
            this.usTask61.Name = "usTask61";
            this.usTask61.Size = new System.Drawing.Size(857, 640);
            this.usTask61.TabIndex = 8;
            this.usTask61.Visible = false;
            // 
            // usTask51
            // 
            this.usTask51.Location = new System.Drawing.Point(284, 136);
            this.usTask51.Name = "usTask51";
            this.usTask51.Size = new System.Drawing.Size(855, 641);
            this.usTask51.TabIndex = 7;
            this.usTask51.Visible = false;
            // 
            // usTask41
            // 
            this.usTask41.Location = new System.Drawing.Point(284, 136);
            this.usTask41.Name = "usTask41";
            this.usTask41.Size = new System.Drawing.Size(855, 640);
            this.usTask41.TabIndex = 6;
            this.usTask41.Visible = false;
            // 
            // usTask31
            // 
            this.usTask31.Location = new System.Drawing.Point(282, 136);
            this.usTask31.Name = "usTask31";
            this.usTask31.Size = new System.Drawing.Size(857, 640);
            this.usTask31.TabIndex = 5;
            this.usTask31.Visible = false;
            // 
            // usTask21
            // 
            this.usTask21.Location = new System.Drawing.Point(282, 136);
            this.usTask21.Name = "usTask21";
            this.usTask21.Size = new System.Drawing.Size(857, 640);
            this.usTask21.TabIndex = 4;
            this.usTask21.Visible = false;
            // 
            // usHome1
            // 
            this.usHome1.Location = new System.Drawing.Point(282, 435);
            this.usHome1.Name = "usHome1";
            this.usHome1.Size = new System.Drawing.Size(857, 189);
            this.usHome1.TabIndex = 3;
            this.usHome1.Visible = false;
            // 
            // usTask11
            // 
            this.usTask11.BackColor = System.Drawing.Color.White;
            this.usTask11.Location = new System.Drawing.Point(282, 136);
            this.usTask11.Name = "usTask11";
            this.usTask11.Size = new System.Drawing.Size(857, 640);
            this.usTask11.TabIndex = 2;
            this.usTask11.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1137, 820);
            this.Controls.Add(this.usTask11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.usTask61);
            this.Controls.Add(this.usTask51);
            this.Controls.Add(this.usTask41);
            this.Controls.Add(this.usTask31);
            this.Controls.Add(this.usTask21);
            this.Controls.Add(this.usHome1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "AMONG US TASK";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbHome1;
        private System.Windows.Forms.RadioButton rbTask5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton rbTask4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton rbTask3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton rbTask2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel8;
        private UsTask1 usTask11;
        private System.Windows.Forms.RadioButton rbTask1;
        private System.Windows.Forms.RadioButton rbHome;
        private System.Windows.Forms.RadioButton rbTask6;
        private UsHome usHome1;
        private UsTask2 usTask21;
        private UsTask3 usTask31;
        private UsTask4 usTask41;
        private UsTask5 usTask51;
        private UsTask6 usTask61;
    }
}

