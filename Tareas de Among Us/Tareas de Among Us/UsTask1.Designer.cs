﻿
namespace Tareas_de_Among_Us
{
    partial class UsTask1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsTask1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTask1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnGreen = new System.Windows.Forms.Button();
            this.btnred = new System.Windows.Forms.Button();
            this.btnFill = new System.Windows.Forms.Button();
            this.PNLBACk = new System.Windows.Forms.Panel();
            this.PnlFuel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tmrFill = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.PNLBACk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lblTask1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.34375F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 77.65625F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(857, 640);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTask1
            // 
            this.lblTask1.BackColor = System.Drawing.Color.DimGray;
            this.lblTask1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTask1.Font = new System.Drawing.Font("Lucida Fax", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTask1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTask1.Location = new System.Drawing.Point(3, 0);
            this.lblTask1.Name = "lblTask1";
            this.lblTask1.Size = new System.Drawing.Size(851, 143);
            this.lblTask1.TabIndex = 0;
            this.lblTask1.Text = "TAREA NUMERO 1";
            this.lblTask1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 146);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(851, 491);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Controls.Add(this.btnGreen);
            this.panel1.Controls.Add(this.btnred);
            this.panel1.Controls.Add(this.btnFill);
            this.panel1.Controls.Add(this.PNLBACk);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(845, 485);
            this.panel1.TabIndex = 0;
            // 
            // btnGreen
            // 
            this.btnGreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGreen.Enabled = false;
            this.btnGreen.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnGreen.FlatAppearance.BorderSize = 3;
            this.btnGreen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGreen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGreen.Location = new System.Drawing.Point(633, 313);
            this.btnGreen.Name = "btnGreen";
            this.btnGreen.Size = new System.Drawing.Size(22, 21);
            this.btnGreen.TabIndex = 3;
            this.btnGreen.UseVisualStyleBackColor = false;
            // 
            // btnred
            // 
            this.btnred.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnred.Enabled = false;
            this.btnred.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnred.FlatAppearance.BorderSize = 3;
            this.btnred.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnred.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnred.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnred.Location = new System.Drawing.Point(585, 313);
            this.btnred.Name = "btnred";
            this.btnred.Size = new System.Drawing.Size(22, 21);
            this.btnred.TabIndex = 3;
            this.btnred.UseVisualStyleBackColor = false;
            // 
            // btnFill
            // 
            this.btnFill.BackColor = System.Drawing.Color.Silver;
            this.btnFill.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnFill.FlatAppearance.BorderSize = 2;
            this.btnFill.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFill.Font = new System.Drawing.Font("Lucida Fax", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFill.Location = new System.Drawing.Point(591, 351);
            this.btnFill.Name = "btnFill";
            this.btnFill.Size = new System.Drawing.Size(58, 54);
            this.btnFill.TabIndex = 2;
            this.btnFill.Text = "FILL";
            this.btnFill.UseVisualStyleBackColor = false;
            this.btnFill.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnFill_MouseDown_1);
            this.btnFill.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnFill_MouseUp);
            // 
            // PNLBACk
            // 
            this.PNLBACk.BackColor = System.Drawing.Color.Black;
            this.PNLBACk.Controls.Add(this.PnlFuel);
            this.PNLBACk.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.PNLBACk.Location = new System.Drawing.Point(241, 49);
            this.PNLBACk.Name = "PNLBACk";
            this.PNLBACk.Size = new System.Drawing.Size(264, 293);
            this.PNLBACk.TabIndex = 1;
            // 
            // PnlFuel
            // 
            this.PnlFuel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(156)))), ((int)(((byte)(2)))));
            this.PnlFuel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlFuel.Location = new System.Drawing.Point(0, 283);
            this.PnlFuel.Name = "PnlFuel";
            this.PnlFuel.Size = new System.Drawing.Size(264, 10);
            this.PnlFuel.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(229, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(444, 389);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tmrFill
            // 
            this.tmrFill.Tick += new System.EventHandler(this.tmrFill_Tick);
            // 
            // UsTask1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UsTask1";
            this.Size = new System.Drawing.Size(857, 640);
            this.Load += new System.EventHandler(this.UsTask1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.PNLBACk.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblTask1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFill;
        private System.Windows.Forms.Panel PNLBACk;
        private System.Windows.Forms.Panel PnlFuel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer tmrFill;
        private System.Windows.Forms.Button btnGreen;
        private System.Windows.Forms.Button btnred;
    }
}
