﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tareas_de_Among_Us
{
    public partial class UsTask3 : UserControl
    {
        int NAC = 0;

        public UsTask3()
        {
            InitializeComponent();
        }

        private void UsTask3_Load(object sender, EventArgs e)
        {
          int[] Ns = new int[10];

            var rnd = new Random();

            int i = 0;
            while (i < Ns.Length)
            {
                  int NA = rnd.Next(1,11);

                bool EX = false;

                for (int j = 0; j < i; j++)
                {
                    if (NA == Ns[j])
                    {
                        EX = true;
                        break;
                    }
                }
                if (EX ==false)
                {
                    Ns[i] = NA;
                    i++;
                }
            }

            checkBox1.Text = Ns[0].ToString();
            checkBox2.Text = Ns[1].ToString();
            checkBox3.Text = Ns[2].ToString();
            checkBox4.Text = Ns[3].ToString();
            checkBox5.Text = Ns[4].ToString();
            checkBox6.Text = Ns[5].ToString();
            checkBox7.Text = Ns[6].ToString();
            checkBox8.Text = Ns[7].ToString();
            checkBox9.Text = Ns[8].ToString();
            checkBox10.Text = Ns[9].ToString();
        }

        private void btnreset_Click(object sender, EventArgs e)
        {
            UsTask3_Load(sender, e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkPress = (CheckBox)sender;

            if (chkPress.Checked == true)
            {
                int NPress = Convert.ToInt32(chkPress.Text);
                if (NPress == NAC +1)
                {
                    NAC = NPress;
                    if (NAC == 10)
                    {
                        checkBox1.BackColor = Color.Lime;
                        checkBox2.BackColor = Color.Lime;
                        checkBox3.BackColor = Color.Lime;
                        checkBox4.BackColor = Color.Lime;
                        checkBox5.BackColor = Color.Lime;
                        checkBox6.BackColor = Color.Lime;
                        checkBox7.BackColor = Color.Lime;
                        checkBox8.BackColor = Color.Lime;
                        checkBox9.BackColor = Color.Lime;
                        checkBox10.BackColor = Color.Lime;

                        checkBox1.Enabled = false;
                        checkBox2.Enabled = false;
                        checkBox3.Enabled = false;
                        checkBox4.Enabled = false;
                        checkBox5.Enabled = false;
                        checkBox6.Enabled = false;
                        checkBox7.Enabled = false;
                        checkBox8.Enabled = false;
                        checkBox9.Enabled = false;
                        checkBox10.Enabled = false;
                    }
                }
                else
                {
                    checkBox1.BackColor = Color.Red;
                    checkBox2.BackColor = Color.Red;
                    checkBox3.BackColor = Color.Red;
                    checkBox4.BackColor = Color.Red;
                    checkBox5.BackColor = Color.Red;
                    checkBox6.BackColor = Color.Red;
                    checkBox7.BackColor = Color.Red;
                    checkBox8.BackColor = Color.Red;
                    checkBox9.BackColor = Color.Red;
                    checkBox10.BackColor = Color.Red;

                    checkBox1.Checked = false;
                    checkBox2.Checked = false;
                    checkBox3.Checked = false;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox6.Checked = false;
                    checkBox7.Checked = false;
                    checkBox8.Checked = false;
                    checkBox9.Checked = false;
                    checkBox10.Checked = false;
                    NAC = 0;
                }
            }
        }
    }
}
