﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tareas_de_Among_Us
{
    public partial class UsTask1 : UserControl
    {
        public UsTask1()
        {
            InitializeComponent();
        }

        private void UsTask1_Load(object sender, EventArgs e)
        {
            PnlFuel.Height = 0;
        }
                private void btnFill_MouseDown_1(object sender, MouseEventArgs e)
        {
            tmrFill.Enabled = true;
            btnred.BackColor = Color.Red;
        }

        private void btnFill_MouseUp(object sender, MouseEventArgs e)
        {
            tmrFill.Enabled = false;
            btnred.BackColor = Color.FromArgb (64, 0, 0);
        }

        private void tmrFill_Tick(object sender, EventArgs e)
        {
            if (PnlFuel.Height < PNLBACk.Height)
            {
                PnlFuel.Height += 4;
            }
            else
            {
                btnGreen.BackColor = Color.Green;
                btnred.BackColor = Color.FromArgb(64, 0, 0);
                btnFill.Enabled = false;
                tmrFill.Enabled = false;
            }
        }


    }
}
