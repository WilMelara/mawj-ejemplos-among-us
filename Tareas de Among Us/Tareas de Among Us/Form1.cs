﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tareas_de_Among_Us
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void rbHome1_CheckedChanged(object sender, EventArgs e)
        {
            usHome1.Visible = rbHome1.Checked;
        }

        private void rbTask1_CheckedChanged(object sender, EventArgs e)
        {
            usTask11.Visible = rbTask1.Checked;
        }

        private void rbTask2_CheckedChanged(object sender, EventArgs e)
        {
            usTask21.Visible = rbTask2.Checked;
        }

        private void rbTask3_CheckedChanged(object sender, EventArgs e)
        {
            usTask31.Visible = rbTask3.Checked;
        }

        private void rbTask4_CheckedChanged(object sender, EventArgs e)
        {
            usTask41.Visible = rbTask4.Checked;
        }

        private void rbTask5_CheckedChanged(object sender, EventArgs e)
        {
            usTask51.Visible = rbTask5.Checked;
        }

        private void rbTask6_CheckedChanged(object sender, EventArgs e)
        {
            usTask61.Visible = rbTask6.Checked;
        }
    }
}
